@extends('front.layout')

@section('title', $title)

@section('content')

    @include('front._include.top_tabs')

    <div class="block_card">
        <div class="activity_card">
            <div class="head">
                <div class="date">
                    <p>26</p>
                    <span>ноября</span>
                </div>
                <div class="info">
                    <p class="text_green">Рекомендуется</p>
                    <p class="clock"> <img src="/images/icons/clock.svg" alt=""> 14:00</p>
                    <p class="adres"> <img src="/images/icons/location.svg" alt=""> Атырау, ул. Бергалиева 26</p>
                </div>
            </div>
            <div class="body">
                <a class="title">День цифрозизации Министерство информации и коммуникаций Республики Казахстан</a>
                <p>День цифрозизации Министерство информации и коммуникаций Республики Казахстан ...</p>
            </div>
            <div class="dbl_button">
                <a href="/view" class="button_green">Пойду</a>
                <a href="#" class="button_white">Не пойду</a>
            </div>
        </div>
        <div class="activity_card">
            <div class="head">
                <div class="date">
                    <p>26</p>
                    <span>ноября</span>
                </div>
                <div class="info">
                    <p class="text_green">Рекомендуется</p>
                    <p class="clock"> <img src="/images/icons/clock.svg" alt=""> 14:00</p>
                    <p class="adres"> <img src="/images/icons/location.svg" alt=""> Атырау, ул. Бергалиева 26</p>
                </div>
            </div>
            <div class="body">
                <a class="title">День цифрозизации Министерство информации и коммуникаций Республики Казахстан</a>
                <p>День цифрозизации Министерство информации и коммуникаций Республики Казахстан ...</p>
            </div>
            <div class="dbl_button">
                <a href="#" class="button_green">Пойду</a>
                <a href="#" class="button_red">Не пойду</a>
            </div>
        </div>
        <div class="activity_card">
            <div class="head">
                <div class="date">
                    <p>26</p>
                    <span>ноября</span>
                </div>
                <div class="info">
                    <p class="text_green">Рекомендуется</p>
                    <p class="clock"> <img src="/images/icons/clock.svg" alt=""> 14:00</p>
                    <p class="adres"> <img src="/images/icons/location.svg" alt=""> Атырау, ул. Бергалиева 26</p>
                </div>
            </div>
            <div class="body">
                <a class="title">День цифрозизации Министерство информации и коммуникаций Республики Казахстан</a>
                <p>День цифрозизации Министерство информации и коммуникаций Республики Казахстан ...</p>
            </div>
            <div class="dbl_button">
                <a href="#" class="button_green">Пойду</a>
                <a href="#" class="button_red">Не пойду</a>
            </div>

            <div class="execution">
                <p>Исполнение</p>
                <div class="dbl_button m_5">
                    <a href="#" class="button_blue">Опубликовано</a>
                    <a href="#" class="button_white">Не опубликовано</a>
                </div>
                <div class="link">
                    <div>
                        <p>Ссылка</p>
                        <span>https://dribbble.com</span>
                    </div>
                    <img src="./images/icons/copy.svg" alt="">
                </div>
                <div class="error_alert">
                    <p>Если материал был опубликован, укажите пожалуйста ссылку или номер выпуска газеты/новостей</p>
                    <img src="/images/icons/error.svg" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection