@extends('front.layout')

@section('title', $title)

@section('content')

    <div class="view_block">
        <div class="head">
            <div class="date">
                <p>26</p>
                <span>ноября</span>
            </div>
            <div class="info">
                <p class="text_green">Рекомендуется</p>
                <p class="clock"> <img src="/images/icons/clock.svg" alt=""> 14:00</p>
                <p class="adres"> <img src="/images/icons/location.svg" alt=""> Атырау, ул. Бергалиева 26</p>
            </div>
        </div>
        <div class="title">
            <h1>День цифрозизации Министерство информации и коммуникаций Республики Казахстан</h1>
        </div>
        <div class="center_text">
            <p>Организатор:</p>
            <span>Министерство информации и комуникаций Республики Казахстан</span>
        </div>      
        <div class="execution">
            <p>Исполнение</p>
            <div class="dbl_button m_5">
                <a href="#" class="button_blue">Опубликовано</a>
                <a href="#" class="button_white">Не опубликовано</a>
            </div>
            <div class="link">
                <div>
                    <p>Ссылка</p>
                    <span>https://dribbble.com</span>
                </div>
                <img src="./images/icons/copy.svg" alt="">
            </div>
            <div class="error_alert">
                <p>Если материал был опубликован, укажите пожалуйста ссылку или номер выпуска газеты/новостей</p>
                <img src="/images/icons/error.svg" alt="">
            </div>
            <div class="document_block">
                <h3>Документация</h3>

                @for($i = 0; $i < 7; $i++)
                    <div class="document_card">
                        <input type="checkbox" name="" id="chack_{{$i}}">
                        <label for="chack_{{$i}}">
                            <p>Документ</p>
                            <span>Размер: 256kb</span>
                        </label>
                        <a href="#"><img src="/images/icons/download.svg" alt=""></a>
                    </div>
                @endfor

                <a href="#" class="button_blue w-100">Скачать все</a>
            </div>
        </div>  
        <div class="body">
            <p>Не следует, однако забывать, что начало повседневной работы по формированию позиции требуют определения и уточнения новых предложений. Идейные соображения высшего порядка, а также рамки и место обучения кадров требуют определения и уточнения форм развития. </p>
            <p>Равным образом рамки и место обучения кадров играет важную роль в формировании модели развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что сложившаяся структура организации влечет за собой процесс внедрения и модернизации дальнейших направлений развития.</p>
        </div>
    </div>

@endsection