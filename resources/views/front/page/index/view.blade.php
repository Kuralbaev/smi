@extends('front.layout')

@section('title', $title)

@section('content')

    <div class="view_block">
        <div class="head">
            <div class="date">
                <p>26</p>
                <span>ноября</span>
            </div>
            <div class="info">
                <p class="text_green">Рекомендуется</p>
                <p class="clock"> <img src="/images/icons/clock.svg" alt=""> 14:00</p>
                <p class="adres"> <img src="/images/icons/location.svg" alt=""> Атырау, ул. Бергалиева 26</p>
            </div>
        </div>
        <div class="center_text">
            <p>Организатор:</p>
            <span>Министерство информации и комуникаций Республики Казахстан</span>
        </div>
        <div class="body">
            <div class="dbl_button">
                <a href="#" class="button_green">Пойду</a>
                <a href="#" class="button_white">Не пойду</a>
            </div>
            <h3>День цифрозизации Министерство информации и коммуникаций Республики Казахстан</h3>
            <p>Не следует, однако забывать, что начало повседневной работы по формированию позиции требуют определения и уточнения новых предложений. Идейные соображения высшего порядка, а также рамки и место обучения кадров требуют определения и уточнения форм развития. </p>
            <p>Равным образом рамки и место обучения кадров играет важную роль в формировании модели развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что сложившаяся структура организации влечет за собой процесс внедрения и модернизации дальнейших направлений развития.</p>
        </div>
        <div class="document_block">
            <h3>Документация</h3>

            @for($i = 0; $i < 7; $i++)
                <div class="document_card">
                    <input type="checkbox" name="" id="chack_{{$i}}">
                    <label for="chack_{{$i}}">
                        <p>Документ</p>
                        <span>Размер: 256kb</span>
                    </label>
                    <a href="#"><img src="/images/icons/download.svg" alt=""></a>
                </div>
            @endfor

            <a href="#" class="button_blue w-100">Скачать все</a>
        </div>
    </div>

@endsection