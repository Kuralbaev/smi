<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=width-device, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/images/icons/favicon.ico" type="image/ico">

    <title> {{ $title }} </title>

    <link rel="stylesheet" href="/fontawesome/font-awesome.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    @yield('style')

    <script src="/js/jquery.min.js"></script>
    <script src="/js/select2.js"></script>


</head>
<body>
    
    

    <!-- Content -->
    @yield('content')

    
    <script src="{{ mix('/js/app.js') }}"></script>
    @yield('script')

    </body>
</html>