<div class="top_tabs">
    <h1>{{ $title }}</h1>

    <div class="tabs">
        <ul>
            <li class="active"><a href="#">Все</a></li>
            <li><a href="#">Посещенные</a></li>
            <li><a href="#">Прошедшие</a></li>
        </ul>
    </div>
</div>  