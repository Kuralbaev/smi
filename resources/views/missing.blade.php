<?php
$title = 'Такой страницы не существует'; ?>

@extends('front.layout')

@section('title', $title)

@section('content')
<div class="loginBg">
    <div class="loginBg__blue"></div>
    <div class="loginBg__white"></div>
</div>
<div class="main main--noscroll">
    <div class="card mar_top card--profile card--404">
        <div class="card-header">
            {{ $title }}
        </div>

        <div class="card-body">
            <p class="title_404">404</p>
            <span>Ошибка. Такой страницы не существует. <br>Пожалуйста вернитесь на <a href="/" class="content_link"> Главную </a></span>
        </div>
    </div>
    @include('front.__include.sidebar')
</div>

@endsection
