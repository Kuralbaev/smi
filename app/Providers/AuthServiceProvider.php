<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


use App\Model\News;
use App\Policies\Admin\News\LibNewsPolicy;


use App\Model\LibBudgetCat;
use App\Model\LibBudgetSubcat;
use App\Model\LibBudgetSubcatOption;
use App\Model\LibBudgetExplain;
use App\Model\LibNewsCat;
use App\Model\LibOrganCat;
use App\Model\LibOrganSubcat;
use App\Model\LibProposeCat;
use App\Policies\Admin\Lib\LibCommonPolicy;

use App\Model\Director;
use App\Model\Company;
use App\Policies\Admin\Lib\LibOrganPolicy;

use App\Model\View\SystemModuleRelation;
use App\Model\View\SystemAuhLog;
use App\Model\View\SystemSysEditLog;
use App\Model\View\SystemControlDiff;
use App\Model\Blog;
use App\Model\SysCity;
use App\User;
use App\Policies\Admin\System\SystemCommonPolicy;

use App\Model\Organ;
use App\Policies\Admin\Organ\OrganCommonPolicy;

use App\Model\Budget;
use App\Policies\Admin\Budget\BudgetCommonPolicy;

use App\Model\Appeal;
use App\Policies\Admin\Appeal\AppealCommonPolicy;

use App\Model\View\SystemControlFill;
use App\Policies\Admin\System\SystemControlPolicy;

use App\Model\View\User as  UserView;
use App\Policies\Admin\System\SystemGosUserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        News::class                         => LibNewsPolicy::class,


        LibBudgetCat::class                 => LibCommonPolicy::class,
        LibBudgetSubcat::class              => LibCommonPolicy::class,
        LibBudgetSubcatOption::class        => LibCommonPolicy::class,
        LibBudgetExplain::class             => LibCommonPolicy::class,
        LibNewsCat::class                   => LibCommonPolicy::class,
        LibOrganCat::class                  => LibCommonPolicy::class,
        LibOrganSubcat::class               => LibCommonPolicy::class,
        LibProposeCat::class                => LibCommonPolicy::class,

        Director::class                     => LibOrganPolicy::class,
        Company::class                      => LibOrganPolicy::class,

        Blog::class                         => SystemCommonPolicy::class,
        SysCity::class                      => SystemCommonPolicy::class,
        User::class                         => SystemCommonPolicy::class,
        SystemModuleRelation::class         => SystemCommonPolicy::class,
        SystemAuhLog::class                 => SystemCommonPolicy::class,
        SystemSysEditLog::class             => SystemCommonPolicy::class,
        SystemControlDiff::class            => SystemCommonPolicy::class,
        
        SystemControlFill::class            => SystemControlPolicy::class,

        UserView::class                     => SystemGosUserPolicy::class,
        
        Organ::class                        => OrganCommonPolicy::class,

        Budget::class                       => BudgetCommonPolicy::class,

        Appeal::class                       => AppealCommonPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(){
        $this->registerPolicies();
    }
}
