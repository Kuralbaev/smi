<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Model\SysUserType;

class UserAuth {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        $ar_can_access = [ SysUserType::ADMIN_TYPE, SysUserType::USER_TYPE];

        if ($this->auth->guest() || !in_array($this->auth->user()->type_id, $ar_can_access)){
            Auth::logout();
            
            return redirect()->action('Front\LoginController@getIndex')->with('error', 'Введите email и пароль, для доступа в админку');
        }

        return $next($request);
    }
}
