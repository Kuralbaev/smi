<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Model\SysUserType;

class AdminAuth {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        $ar_can_access = [ SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE, SysUserType::CHIEF_TYPE, SysUserType::WORKER_ORGAN_TYPE, SysUserType::WORKER_TERR_DEP_TYPE];

        if ($this->auth->guest() || !in_array($this->auth->user()->type_id, $ar_can_access)){
            Auth::logout();
            
            return redirect()->action('Admin\AuthController@getLogin')->with('error', 'Введите email и пароль, для доступа в кабинет');
        }

        return $next($request);
    }
}
