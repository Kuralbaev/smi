<?php

namespace App\Http\Requests\Admin\Lib\BudgetSubcatOption;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'sub_cat_id' => 'required',
            'sys_key' => 'required',
        ];
    }
}
