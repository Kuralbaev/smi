<?php

namespace App\Http\Requests\Admin\Lib\Company;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|alpha_spaces|max:255',
            'directors' => 'required|alpha_spaces|max:255',
            'address' => 'required|alpha_spaces|max:255',
            'bin' => 'required|alpha_spaces|max:255',
            'phone' => 'required|alpha_spaces|max:255',
            'email' => 'required|email|max:255',
            'photo' => 'mimes:jpeg,bmp,png,jpg'
        ];
    }
}
