<?php

namespace App\Http\Requests\Admin\Appeal\Appeal;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\SysAppealStatus;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => 'required|in:'.SysAppealStatus::IN_WORK_STATUS.','.SysAppealStatus::CLOSE_STATUS.'',
            'note' => 'required',
        ];
    }
}
