<?php

namespace App\Http\Requests\Admin\Budget\Comment;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'budget_id' => 'required',
            'title' => 'required|max:255',
            'parent_id' => 'required',
            'note' => 'required',
        ];
    }
}
