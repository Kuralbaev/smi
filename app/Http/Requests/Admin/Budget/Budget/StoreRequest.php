<?php

namespace App\Http\Requests\Admin\Budget\Budget;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|max:255',
            'city_id' => 'required',
            'cat_id' => 'required',
            'subcat_id' => 'required',
        ];
    }
}
