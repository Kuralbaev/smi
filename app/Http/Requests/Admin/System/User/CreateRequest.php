<?php

namespace App\Http\Requests\Admin\System\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:255',
            'type_id' => 'required|integer',
            'email' => 'required|max:255|email|unique:users',
            'password' => 'required|max:255|min:6',
        ];
    }
}
