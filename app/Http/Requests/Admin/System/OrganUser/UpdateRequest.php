<?php

namespace App\Http\Requests\Admin\System\OrganUser;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|alpha_spaces|max:255',
            'password' => 'max:255',
            'photo' => 'mimes:jpeg,bmp,png,jpg',
            'organ_id' => 'required|integer',

            'phone' => 'nullable|alpha_spaces',
            'mobile' => 'nullable|alpha_spaces',
            'address' => 'nullable|alpha_spaces',
            'note' => 'nullable|alpha_spaces',
            'position' => 'nullable|alpha_spaces'
        ];
    }
}
