<?php

namespace App\Http\Requests\Admin\Organ\Organ;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|max:255',
            'city_id' => 'required',
            'cat_id' => 'required',
            'director_id' => 'required',
            'geo_x' => 'required',
            'geo_y' => 'required',
        ];
    }
}
