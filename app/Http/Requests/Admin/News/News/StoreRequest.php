<?php

namespace App\Http\Requests\Admin\News\News;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|max:255',
            'short_note' => 'required',
            'note' => 'required',
            'cat_id' => 'required',
            'date_news' => 'required|date'
        ];
    }
}
