<?php  namespace App\Http\Controllers\Front\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Model\Budget;
use App\Model\LibBudgetCat;
use App\Model\LibBudgetSubcat;
use App\Model\LibBudgetExplain;
use App\Model\SysBudgetStatus;
use App\Services\ModuleRelation;
use App\Model\BudgetComment;
use App\Model\Appeal;
use App\Services\UploadPhoto;

use App\Model\ModuleRelation as DBModuleRelation;

class ComplaintController extends Controller{
    function getIndex(Request $request, Budget $item){
        $items = Budget::where('id', '>', 0);
        
        $ar = array();
        $ar['title'] = 'Интерактивная карта общественного контроля';
        $ar['items'] = $items->orderBy('id', 'desc')->with('relData')->paginate(16);
        $ar['request'] = $request;

        $ar['ar_cat'] = LibBudgetCat::pluck('name', 'id')->toArray();
        $ar['ar_explain'] = LibBudgetExplain::pluck('name', 'id')->toArray();
        $ar['ar_status'] = SysBudgetStatus::pluck('name', 'id')->toArray();
        $ar['ar_subcat'] = LibBudgetSubcat::pluck('name', 'id')->toArray();
        
        return view('front.page.budget.complaint.index', $ar);
    }
}