<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontController extends Controller
{
    // Main
    function getIndex(Request $request){
        $ar = array();
        $ar['title'] = 'Мероприятия';

        return view('front.page.index.index', $ar);
    }

    // View
    function getIndex_view(Request $request){
        $ar = array();
        $ar['title'] = 'Мероприятия';

        return view('front.page.index.view', $ar);
    }

    // View v2
    function getIndex_view_2(Request $request){
        $ar = array();
        $ar['title'] = 'Мероприятия';

        return view('front.page.index.view_2', $ar);
    }
}
