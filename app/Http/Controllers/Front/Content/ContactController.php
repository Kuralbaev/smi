<?php  namespace App\Http\Controllers\Front\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Model\Budget;
use App\Model\LibBudgetCat;
use App\Model\LibBudgetSubcat;
use App\Model\LibBudgetExplain;
use App\Model\SysBudgetStatus;
use App\Services\ModuleRelation;
use App\Model\BudgetComment;
use App\Model\Appeal;
use App\Services\UploadPhoto;

use App\Model\ModuleRelation as DBModuleRelation;

class ContactController extends Controller{
    function getIndex(Request $request, Budget $item){
        $items = Budget::where('id', '>', 0);
        
        $ar = array();
        $ar['title'] = 'Контакты';
        $ar['items'] = $items->orderBy('id', 'desc')->with('relData')->paginate(16);
        $ar['request'] = $request;
        
        return view('front.page.content.contact.index', $ar);
    }
}