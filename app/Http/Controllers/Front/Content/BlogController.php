<?php
namespace App\Http\Controllers\Front\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Blog;

class BlogController extends Controller{
    function getIndex (Request $request){
        $ar = array();
        $ar['title'] = 'Блог';
        $ar['items'] = Blog::where('is_active', 1)->latest()->paginate(24);
        $ar['request'] = $request;

        return view('front.page.content.blog.index', $ar);
    }

    function getView(Request $request, Blog $item){
        $ar = array();
        $ar['title'] = $item->title;
        $ar['item'] = $item;

        return view('front.page.content.blog.view', $ar);
    }

    
}
