<?php
namespace App\Services;

class UploadFile {
    static function upload($file){
        if (!$file)
            return null;

        $file_name =  time().rand(0,9).'.'.$file->getClientOriginalExtension();
        
        return $file->storeAs('store/'.date('Y').'/'.date('m').'/'.date('d'), $file_name);
    }
}