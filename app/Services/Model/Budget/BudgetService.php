<?php
namespace App\Services\Model\Budget;

use App\Services\Model\ParentService;

use App\Model\SysUserType;

use App\Model\Budget;
use App\Model\BudgetData;

use App\ListGenerator\Model\SysCityList;
use App\ListGenerator\Model\SysRegionList;
use App\ListGenerator\Model\SysBudgetStatusList;
use App\ListGenerator\Model\LibBudgetCatList;
use App\ListGenerator\Model\LibBudgetSubcatList;
use App\ListGenerator\Model\LibBudgetExplainList;
use App\ListGenerator\Model\CompanyList;
use App\ListGenerator\Model\OrganList;
use App\ListGenerator\Model\ControlOrganList;
use App\ListGenerator\Model\BudgetList;
use App\ListGenerator\Model\ControlOrganSpecialList;

use App\Model\ModuleRelation;
use App\Services\UploadPhoto;
use App\Model\Organ;

class BudgetService extends ParentService {
    
    function __construct(){
        $this->city = new SysCityList;
        $this->region = new SysRegionList;
        $this->status = new SysBudgetStatusList;
        $this->cat = new LibBudgetCatList;
        $this->subcat = new LibBudgetSubcatList;
        $this->explain = new LibBudgetExplainList;
        $this->company = new CompanyList;
        $this->organ = new OrganList;
        $this->control_organ = new ControlOrganList;
        $this->budget = new BudgetList;
        $this->organ_special = new ControlOrganSpecialList();
    }

    private function getList(){
        $request = $this->request;

        $items = $this->budget->getRaw();
		if ($request->has('name') && $request->name)
			$items->where('name', 'like', '%'.$request->name.'%');
		
		if ($request->has('status_id') && $request->status_id)
			$items->where('status_id', $request->status_id);
		
		if ($request->has('subcat_id') && $request->subcat_id)
            $items->where('subcat_id', $request->subcat_id);
            
        if ($request->has('cat_id') && $request->cat_id)
			$items->where('cat_id', $request->cat_id);
	
		if ($request->has('company_id') && $request->company_id)
            $items->where('company_id', $request->company_id);
            

        $this->items = $items->latest()->paginate(24);
        
        return $this->items;
    }



    function generateForList(){
        $ar = [];
        $ar['items'] =  $this->getList();
        $ar['request'] = $this->request;
        
        $ar['ar_city'] = $this->city->getAr();
        $ar['regions'] = $this->region->getAr();
        $ar['ar_status'] = $this->status->getAr();
        $ar['ar_cat'] = $this->cat->getAr();
        $ar['ar_subcat'] = $this->subcat->getAr();
        $ar['ar_company'] = $this->company->getAr();
        $ar['ar_organ'] =  $this->organ->getAr();

        return $ar;
    }


    function generateForStore(){
        $ar = [];
        $ar['ar_city'] = $this->city->getAr();
        $ar['regions'] = $this->region->getList();
        $ar['ar_status'] = $this->status->getAr();
        $ar['ar_cat'] = $this->cat->getAr();
        $ar['subcats'] = $this->subcat->getList();
        $ar['ar_company'] = $this->company->getAr();
        $ar['ar_organ'] =  $this->organ->getAr();
        $ar['ar_explain'] =  $this->explain->getAr();
        $ar['control_organ'] = $this->organ_special->getAr();

        $ar['request'] = $this->request;

        return $ar;
    }

    private function validateRequestForStore(){
        $request = $this->request;
        $ar_city = array_keys($this->city->getAr());
        if (!in_array($request->city_id, $ar_city))
            throw new \Exception('Указанный город не корректен');

        $ar_region = array_keys($this->region->getAr());
        if ($request->region_id && !in_array($request->region_id, $ar_region))
            throw new \Exception('Указанный регион не корректен');
        
        $ar_organ = array_keys($this->organ->getAr());
        if ($request->organ_id && !in_array($request->organ_id, $ar_organ))
            throw new \Exception('Орган не корректен'); 

    }

    function create(){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        $ar = $request->all();

        $ar['photo'] = UploadPhoto::upload($request->photo);
        $ar['user_id'] = $request->user()->id;
        
        $item = Budget::create($ar);

        $ar['budget_id'] = $item->id;
        $data = BudgetData::create($ar);

        $this->storeModuleRealation(new ModuleRelation(), $item, $request);

    }

    private function storeModuleRealation($rel, $item, $request){
        $rel->budjet_id = $item->id;
        $rel->from_id = $item->id;
        $rel->from_field = 'budjet_id';
        $rel->creator_id = $item->user_id;

        if (!$item->rel_organ){
            $rel->company_id = $request->company_id;
            $rel->organ_id = $request->organ_id;
            $rel->director_id = null;
        }  
        else {
            $rel->company_id = null;
            $rel->organ_id = $request->organ_id;

            $organ = Organ::find($rel->organ_id);
            if ($organ)
                $rel->director_id = $organ->director_id;
        }
        $rel->save();   
    }


    function update ($item){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        $ar = $request->all();
        

        if ($request->photo)
            $ar['photo'] = UploadPhoto::upload($request->photo);
        else {
            unset($ar['photo']);
        }
            
            
        $ar['user_id'] = $request->user()->id;

        $item->update($ar);
        if(!$item->relData->empty){
            $item->relData->update($ar);
        }
        else {
            $ar['budget_id'] = $item->id;
            BudgetData::create($ar);
        }

        if ($item->relModuleRel)
            $this->storeModuleRealation($item->relModuleRel, $item, $request);
        else 
            $this->storeModuleRealation(new ModuleRelation(), $item, $request);
    }
    
}