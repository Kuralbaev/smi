<?php
namespace App\Services\Model\Appeal;

use App\Services\Model\ParentService;

use App\Model\SysUserType;

use App\Model\Appeal;
use App\Model\AppealMessage;
use App\Model\SysAppealStatus;


use App\ListGenerator\Model\AppealList;

use App\Model\Budget;

use App\Services\UploadFile;
use App\Services\ModuleRelation;

class AppealService extends ParentService {
    
    function __construct(){
        $this->appeal = new AppealList;
    }

    private function getList(){
        $request = $this->request;

        $items = $this->appeal->getRaw();
        if ($request->has('status_id') && $request->status_id)
            $items->where('status_id', $request->status_id);

        $this->items = $items;
        
        return $this->items;
    }



    function generateForList(){

        $ar = [];
        $ar['items'] =  $this->getList()->latest()->paginate(24);
        $ar['request'] = $this->request;
        $ar['ar_status'] = SysAppealStatus::orderBy('id', 'asc')->pluck('name', 'id')->toArray();
        

        return $ar;
    }


    function generateForStore(){
        $ar = [];
        $ar['request'] = $this->request;

        return $ar;
    }

    private function validateRequestForStore(){
        $request = $this->request;
        $ar_city = array_keys($this->city->getAr());
        if (!in_array($request->city_id, $ar_city))
            throw new \Exception('Указанный город не корректен');

        $ar_region = array_keys($this->region->getAr());
        if ($request->region_id && !in_array($request->region_id, $ar_region))
            throw new \Exception('Указанный регион не корректен');
        
        $ar_organ = array_keys($this->organ->getAr());
        if ($request->organ_id && !in_array($request->organ_id, $ar_organ))
            throw new \Exception('Орган не корректен'); 

        $ar_control_organ = array_keys($this->control_organ->getAr());
        if ($request->control_organ_id && !in_array($request->control_organ_id, $ar_control_organ))
            throw new \Exception('Курирующая организация не корректена'); 
    }

    function create(){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        $ar = $request->all();

        $ar['photo'] = UploadPhoto::upload($request->photo);
        $ar['user_id'] = $request->user()->id;
        
        $item = Budget::create($ar);

        $ar['budget_id'] = $item->id;
        $data = BudgetData::create($ar);

        $this->storeModuleRealation(new ModuleRelation(), $item, $request);

    }

    function update ($item){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        $ar = $request->all();
        
        if ($request->hasFile('photo'))
            $ar['photo'] = UploadPhoto::upload($request->photo);
        else 
            unset($ar['photo']);
            
        $ar['user_id'] = $request->user()->id;

        $item->update($ar);
        if(!$item->relData->empty){
            $item->relData->update($ar);
        }
        else {
            $ar['budget_id'] = $item->id;
            BudgetData::create($ar);
        }

        if ($item->relModuleRel)
            $this->storeModuleRealation($item->relModuleRel, $item, $request);
        else 
            $this->storeModuleRealation(new ModuleRelation(), $item, $request);
    }
    
}