<?php
namespace App\Services\Model\Organ;

use App\Services\Model\ParentService;

use App\ListGenerator\Model\SysCityList;
use App\ListGenerator\Model\SysRegionList;
use App\ListGenerator\Model\LibOrganCatList;
use App\ListGenerator\Model\LibOrganSubcatList;
use App\ListGenerator\Model\DirectorList;
use App\ListGenerator\Model\ControlOrganList;
use App\ListGenerator\Model\OrganList;
use App\ListGenerator\Model\ControlOrganSpecialList;

use App\Model\Organ;
use App\Model\OrganData;

use App\Services\UploadPhoto;

class OrganService extends ParentService {
    function __construct(){
        $this->city = new SysCityList;
        $this->region = new SysRegionList;
        $this->cat = new LibOrganCatList;
        $this->subcat = new LibOrganSubcatList();
        $this->director = new DirectorList();
        $this->organ = new OrganList();
        $this->organ_special = new ControlOrganSpecialList();
    }

    private function getList(){
        $request = $this->request;
        $organ = $this->organ->getRaw();

        if ($request->has('user_name') && $request->user_name)
            $organ->whereHas('relUser', function($q) use ($request){
                $q->where('full_name', 'like', '%'.$request->user_name.'%');
            });

        if ($request->has('director_name') && $request->director_name)
            $organ->whereHas('relDirector', function($q) use ($request){
                $q->where('name', 'like', '%'.$request->director_name.'%');
            });

        if ($request->has('name') && $request->name)
            $organ->where('name', 'like', '%'.$request->name.'%');
        
        if ($request->has('parent_id') && $request->parent_id)
            $organ->where('parent_id', $request->parent_id);
       

        $this->items = $organ->paginate(24);
        
        return $this->items;
    }

    function generateForList(){
        $ar = [];
        $ar['items'] = $this->getList();

        $ar['ar_city'] = $this->city->getAr();
        $ar['ar_region'] = $this->region->getAr();

        $ar['ar_cat'] = $this->cat->getAr();
        $ar['ar_sub_cat'] = $this->subcat->getAr();
        $ar['ar_director'] = $this->director->getAr();
        $ar['request'] = $this->request;

        return $ar;
    }


    function generateForStore(){
        $ar = [];
        $ar['ar_city'] = $this->city->getAr();
        $ar['regions'] = $this->region->getList();
        $ar['ar_cat'] = $this->cat->getAr();
        $ar['subcats'] = $this->subcat->getList();
        $ar['ar_director'] = $this->director->getAr();
        $ar['ar_parent'] = $this->organ_special->getAr();
        $ar['request'] = $this->request;


        return $ar;

    }

    private function validateRequestForStore(){
        $request = $this->request;
        $ar_city = array_keys($this->city->getAr());
        if (!in_array($request->city_id, $ar_city))
            throw new \Exception('Указанный город не корректен');

        $ar_region = array_keys($this->region->getAr());
        if ($request->region_id && !in_array($request->region_id, $ar_region))
            throw new \Exception('Указанный регион не корректен');
        
       
    }

    function create(){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        if (Organ::where('name', $request->name)->count())
            throw new \Exception( 'Указанная организация уже есть');

        $ar = $request->all();
        
        $ar['photo'] = UploadPhoto::upload($request->photo);
        $ar['user_id'] = $request->user()->id;

        $item = Organ::create($ar);

        $ar['organ_id'] = $item->id;
        $data = OrganData::create($ar);
    }

    function update ($item){
        $request = $this->request;
        try {
            $this->validateRequestForStore();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage());
        }

        $ar = $request->all();
        
        if ($request->hasFile('photo'))
            $ar['photo'] = UploadPhoto::upload($request->photo);
        else 
            unset($ar['photo']);
            
        $ar['user_id'] = $request->user()->id;

        $item->update($ar);
        if(!$item->relData->empty){
            $item->relData->update($ar);
        }
        else {
            $ar['organ_id'] = $item->id;
            OrganData::create($ar);
        }
    }
    
}