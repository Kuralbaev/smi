<?php
namespace App\Services\Model\System;

use App\Services\Model\ParentService;
use Hash;

use App\Model\View\User;
use App\Model\UserData;

use App\Model\SysUserType;
use App\Model\SysCity;
use App\Model\Organ;

use App\Services\UploadPhoto;


class OrganUser extends ParentService{
    private $ar_type = false;
    private $ar_city = false;
    private $regions = false;
    private $ar_organ = false;


    function bindRequest($request){
        $this->request = $request;
        $this->user = $this->request->user();
    }

    function getArType(){
        if (!$this->ar_type){
            $this->ar_type = SysUserType::whereIn('id', [SysUserType::WORKER_ORGAN_TYPE])->pluck('name', 'id')->toArray();
        }

        return $this->ar_type;
    }


    function getArOrgan(){
        if (!$this->ar_organ){
            $ar_organ = Organ::orderBy('name', 'asc');

            if ($this->user->type_id == SysUserType::WORKER_ORGAN_TYPE)
                $ar_organ->whereIn('id', $this->user->getAccessOrgan());
            else if ($this->user->type_id == SysUserType::WORKER_TERR_DEP_TYPE)
                $ar_organ->where('city_id', $this->user->city_id);
            
            $this->ar_organ = $ar_organ->pluck('name', 'id')->toArray();
        }

        return $this->ar_organ;
    }

    function getItems(){
        if (!$this->items){
            $items = User::where('type_id', SysUserType::WORKER_ORGAN_TYPE);

            if ($this->user->type_id == SysUserType::WORKER_ORGAN_TYPE)
                $items->whereIn('organ_id', $this->user->getAccessOrgan());
            else if ($this->user->type_id == SysUserType::WORKER_TERR_DEP_TYPE)
                $items->where('city_id', $this->user->city_id);

            $this->items = $items->latest()->paginate(24);
        }

        return $this->items;
    } 

    function create($request){
        $ar_organ = array_keys($this->getArOrgan());
        if (!in_array($request->organ_id, $ar_organ))
            throw new \Exception('Указанный орган не корректен');

        $organ = Organ::findOrFail($request->organ_id);

        $ar = $request->all();
        $ar['is_active'] = 1;
        $ar['type_id'] = SysUserType::WORKER_ORGAN_TYPE;
        $ar['city_id'] = $organ->city_id;
        $ar['region_id'] = $organ->region_id;
        $ar['password'] = Hash::make($ar['password']);
        $ar['photo'] = UploadPhoto::upload($request->photo);
        
        $user = User::create($ar);

        $ar['user_id'] = $user->id;
        $data = UserData::create($ar);

        return true;
    }


    function update($request, $user){
        $ar_organ = array_keys($this->getArOrgan());
        if (!in_array($request->organ_id, $ar_organ))
            throw new \Exception('Указанный орган не корректен');

        $organ = Organ::findOrFail($request->organ_id);

        $ar = $request->all();
        $ar['is_active'] = 1;
        $ar['type_id'] = SysUserType::WORKER_ORGAN_TYPE;
        $ar['city_id'] = $organ->city_id;
        $ar['region_id'] = $organ->region_id;


        if ($request->has('email'))
            unset($ar['email']);

        if ($request->has('password') && $request->password)
            $ar['password'] = Hash::make($ar['password']);
        else 
            unset($ar['password']);  

        $ar['photo'] = UploadPhoto::upload($request->photo);
        if (!$ar['photo'])
            unset($ar['photo']);

        $user->update($ar);
        $user->relData->update($ar);

        return true;
    }


    function changeActive($request, $user){
        $user->is_active = ($user->is_active ? 0 : 1);
        $user->save();

        return true;
    }


   
}