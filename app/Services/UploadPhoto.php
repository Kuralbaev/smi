<?php
namespace App\Services;
use ImageOptimizer;

class UploadPhoto {
    static function upload($file, $compress = true){
        if (!$file)
            return null;

        $file_name =  time().rand(0,9).'.'.$file->getClientOriginalExtension();
        $file_path = $file->storeAs('store/'.date('Y').'/'.date('m').'/'.date('d'), $file_name);
        

        if ($compress){
            ImageOptimizer::optimize($file_path);
        }
            

        return $file_path;
    }
}