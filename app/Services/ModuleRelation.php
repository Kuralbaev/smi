<?php
namespace App\Services;

use App\Model\ModuleRelation as DBModuleRelation;

use App\User;
use App\Model\News;
use App\Model\Propose;
use App\Model\Budget;
use App\Model\Organ;
use App\Model\Director;
use App\Model\Appeal;
use App\Model\Company;


class ModuleRelation {
    private $income_obj = false;

    private $parent_field = false;
    private $parent_class = false;

    private $db_items = false;

    private $ar_res = [];

    private $ar_class_filed = [
        'User' => 'user_id',
        'News' => 'news_id',
        'Propose' => 'propose_id',
        'Budget' => 'budjet_id',
        'Organ' => 'organ_id',
        'Director' => 'director_id',
        'Appeal' => 'appeal_id',
        'Company' => 'company_id',
    ];

    function __construct($income_obj){
        $this->income_obj = $income_obj;

        $this->calculateParentClass();
        $this->calculateParentField();
    }



    private function calculateParentClass(){
        if (is_object($this->income_obj))
            $this->parent_class = (new \ReflectionClass($this->income_obj))->getShortName();
    }

    private function calculateParentField(){
        if (isset($this->ar_class_filed[$this->parent_class]))
            $this->parent_field = $this->ar_class_filed[$this->parent_class];
    }

    

    public function getRelationObjects(){
        if (count($this->ar_res) == 0)
            $this->calculateRelationObjects();

        return $this->ar_res;
    }

    private function calculateRelationObjects(){
        $this->calculateDBObjects();
        if (!$this->db_items)
            return;
        
        foreach ($this->db_items as $db_item) {
            foreach ($this->ar_class_filed as $class_name => $field_name) {
                if ($field_name == $this->parent_field)
                    continue;
    
                if (!$db_item->$field_name)
                    continue;
                
                $rel_obj = $this->getObjByField($field_name, $db_item->$field_name); 
                if (!$rel_obj)
                    continue;
                
               
                if (!isset($this->ar_res[$class_name]))
                    $this->ar_res[$class_name] = [];
                
                if (isset($this->ar_res[$class_name][$rel_obj->id]))
                    continue;

                $this->ar_res[$class_name][$rel_obj->id] =$rel_obj;
            }
        }
    }

    private function getObjByField($field_name, $id){
        $obj = false;

        if ($field_name == 'user_id')
            $obj = User::find($id);
        else if ($field_name == 'news_id')
            $obj = News::find($id);
        else if ($field_name == 'propose_id')
            $obj = Propose::find($id);
        else if ($field_name == 'budjet_id')
            $obj = Budget::find($id);
        else if ($field_name == 'organ_id')
            $obj = Organ::find($id);
        else if ($field_name == 'director_id')
            $obj = Director::find($id);
        else if ($field_name == 'appeal_id')
            $obj = Appeal::find($id);
        else if ($field_name == 'company_id')
            $obj = Company::find($id);

        return $obj;
    }   

    private function calculateDBObjects(){
        if ($this->parent_field && $this->income_obj->id)
            $this->db_items = DBModuleRelation::where($this->parent_field, $this->income_obj->id)->get();
    }
    
   
}