<?php
namespace App\Services\Traits;
use App\Model\SysEditLog;

trait ChangeModel {
    public static function boot(){
        parent::boot();

        static::updating(function ($el) {
            SysEditLog::createNote($el);
            return true;
        });
    }
}

?>
