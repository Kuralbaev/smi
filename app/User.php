<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use  App\Services\Traits\ChangeModel;
use App\Services\Traits\DateHelper;

use App\Model\Organ;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;
    use ChangeModel, DateHelper;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'photo', 'email', 'password', 'type_id', 'remember_token', 'created_at', 'updated_at', 'is_active', 'uid', 'city_id', 'region_id', 'organ_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	function relData(){
		return $this->hasOne('App\Model\UserData', 'user_id')->withDefault();
    }
    
    public function getSomeDateAttribute($date){
        return $date->format('m-d');
    }

    function getAccessOrgan(){
        $ar = [];

        if ($this->organ_id){
            $ar = Organ::where('parent_id', $this->organ_id)->pluck('id')->toArray();
            $ar[] = $this->organ_id;
        }
        
        return $ar;
    }


}
