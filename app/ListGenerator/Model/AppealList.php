<?php
namespace App\ListGenerator\Model;

use App\ListGenerator\CommonList;

use App\Model\SysUserType;
use App\Model\Appeal;

class AppealList extends CommonList {
    function __construct(){
        $this->model = new Appeal;
    }

    private function generateItems(){
        $this->bindUser();
        
        if (!$this->items){
            $items = $this->model->orderBy('id', 'desc');

            if ($this->user->type_id == SysUserType::WORKER_ORGAN_TYPE)
                $items->where('control_organ_id', $this->user->organ_id)
                        ->whereIn('is_corrupt', [0, null]);
            else if ($this->user->type_id == SysUserType::WORKER_TERR_DEP_TYPE)
                $items->where('city_id', $this->user->city_id);

            $this->items = $items;
        }

        return $this->items;
    }

    public function getList(){
        $this->generateItems();

        return $this->items->get();
    }

    public function getAr(){
        $this->generateItems();

        return $this->items->pluck('name', 'id')->toArray();
    }

    public function getRaw(){
        return  $this->generateItems();
    }
}