<?php
namespace App\ListGenerator\Model;

use App\ListGenerator\CommonList;

use App\Model\SysUserType;
use App\Model\Budget;

class BudgetList extends CommonList {
    function __construct(){
        $this->model = new Budget;
    }

    private function generateItems(){
        $this->bindUser();
        
        if (!$this->items){
            $items = $this->model->orderBy('id', 'desc');

            if ($this->user->type_id == SysUserType::WORKER_ORGAN_TYPE)
                $items->whereIn('organ_id', $this->user->getAccessOrgan());
            else if ($this->user->type_id == SysUserType::WORKER_TERR_DEP_TYPE)
                $items->where('city_id', $this->user->city_id);

            $this->items = $items->latest();
        }

        return $this->items;
    }

    public function getList(){
        $this->generateItems();

        return $this->items->get();
    }

    public function getAr(){
        $this->generateItems();

        return $this->items->pluck('name', 'id')->toArray();
    }

    public function getRaw(){
        return  $this->generateItems();
    }
}