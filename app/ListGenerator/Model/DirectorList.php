<?php
namespace App\ListGenerator\Model;

use App\ListGenerator\CommonList;

use App\Model\SysUserType;
use App\Model\Director;

class DirectorList extends CommonList {
    function __construct(){
        $this->model = new Director;
    }

    private function generateItems(){
        
        if (!$this->items){
            $items = $this->model->orderBy('name', 'asc');
            
            $this->items = $items;
        }

        return $this->items;
    }

    public function getList(){
        $this->generateItems();

        return $this->items->get();
    }

    public function getAr(){
        $this->generateItems();

        return $this->items->pluck('name', 'id')->toArray();
    }
}