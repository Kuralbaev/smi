<?php
namespace App\ListGenerator\Model;

use App\ListGenerator\CommonList;

use App\Model\SysUserType;
use App\Model\SysCity;

class SysCityList extends CommonList {
    function __construct(){
        $this->model = new SysCity;
    }

    private function generateItems(){
        $this->bindUser();

        if (!$this->items){
            $items = $this->model->where('parent_id', 0);

            if ($this->user->type_id == SysUserType::WORKER_ORGAN_TYPE)
                $items->where('id', $this->user->city_id);
            else if ($this->user->type_id == SysUserType::WORKER_TERR_DEP_TYPE)
                $items->where('id', $this->user->city_id);

            $this->items = $items->orderBy('name', 'asc');

        }

        return $this->items;
    }

    public function getList(){
        $this->generateItems();

        return $this->items->get();
    }

    public function getAr(){
        $this->generateItems();


        return $this->items->pluck('name', 'id')->toArray();
    }
}