<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

class Budget extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'budget';
    protected $fillable = [ 'status_id', 'city_id', 'cat_id', 'subcat_id', 'name',  'explain_id', 'is_road', 'line_length',
                            'tender_path', 'date_b', 'date_e', 'photo', 'geo_x', 'geo_y', 'geo_ar', 'organ_id', 'control_organ_id',
                            'summ', 'company_id', 'user_id', 'rel_organ', 'is_many_road', 'geo_ar_many', 'organ_id', 'control_organ_id'];

    protected $dates = ['created_at', 'updated_at', 'date_b', 'date_e'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relControlOrgan(){
        return $this->belongsTo('App\Model\Organ', 'control_organ_id');
    }

    function relModuleRel(){
        return $this->hasOne('App\Model\ModuleRelation', 'from_id', 'id')->where('from_field', 'budjet_id');
    }

    function relData(){
        return $this->hasOne('App\Model\BudgetData', 'budget_id')->withDefault([
            'empty' => true
        ]);
    }

    

    function relOptions(){
        return $this->hasMany('App\Model\BudgetOption', 'budget_id');
    }

    function relComment(){
        return $this->hasMany('App\Model\BudgetComment', 'budget_id');
    }

    function relPhoto(){
        return $this->hasMany('App\Model\BudgetPhoto', 'budget_id');
    }

    function relTechDoc(){
        return $this->hasMany('App\Model\BudgetTechDoc', 'budget_id');
    }

    function relDiff(){
        return $this->hasOne('App\Model\BudgetDiff', 'el_id');
    }

    function getSummStrAttribute(){
        return number_format($this->summ/1000, 0, ',', ' ');
    }

    function relCompany(){
        return $this->belongsTo('App\Model\Company', 'company_id');
    }
    
    
    function getBudgetTimeAttribute(){

        $date_b=$this->date_b;
        $date_e=$this->date_e;

        $date_b=substr($date_b,5,2);
        $date_e=substr($date_e,5,2);


        $date=$date_e-$date_b;

        return $date;
    }

    public function setDateBAttribute($v){
        try {
            $date = new DateTime($v);
            if (!$date){
                $this->attributes['date_b'] =  null;
                return '';
            }
            else if ($date->format('Y') > 9999 && $date->format('Y') < 1000) {
                $this->attributes['date_b'] =  null;
                return '';
            }
        } catch (\Exception $e) {
            $this->attributes['date_b'] =  null;
            return '';
        }

       

        $this->attributes['date_b'] = $date->format('Y-m-d');
    }

    public function setDateEAttribute($v){
        try {
            $date = new DateTime($v);
            if (!$date){
                $this->attributes['date_e'] =  null;
                return '';
            }
            else if ($date->format('Y') > 9999 && $date->format('Y') < 1000) {
                $this->attributes['date_e'] =  null;
                return '';
            }
        } catch (\Exception $e) {
            $this->attributes['date_e'] =  null;
            return '';
        }

        $this->attributes['date_e'] = $date->format('Y-m-d');
    }





}
