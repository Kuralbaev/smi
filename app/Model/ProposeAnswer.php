<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;

class ProposeAnswer extends Model {
    use DateHelper, ChangeModel;

    protected $table = 'propose_answer';
    protected $fillable = [ 'propose_id', 'name', 'note', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
