<?php

namespace App\Model\View;

use App\User as DefUser;


class User extends DefUser
{
   
    function relCity(){
        return $this->belongsTo('App\Model\SysCity', 'city_id');
    }

    function relRegion(){
        return $this->belongsTo('App\Model\SysCity', 'region_id');
    }

    function relOrgan(){
        return $this->belongsTo('App\Model\Organ', 'organ_id');
    }
}
