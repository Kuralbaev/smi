<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganData extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'organ_data';
    protected $fillable = [ 'organ_id', 'address', 'phone', 'note', 'email', 'timetable'];


}
