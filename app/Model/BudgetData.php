<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetData extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'budget_data';
    protected $fillable = [ 'budget_id', 'note', 'address', 'phone', 'email', 'site'];
    
}
