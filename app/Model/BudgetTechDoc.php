<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;

class BudgetTechDoc extends Model {
    use DateHelper, ChangeModel;
    protected $table = 'budget_tech_doc';
    protected $fillable = ['budget_id', 'user_id', 'path', 'name'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
