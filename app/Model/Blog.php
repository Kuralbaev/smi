<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;

class Blog extends Model {
    use DateHelper, ChangeModel;

    protected $table = 'blog';
    protected $fillable = [ 'title', 'note', 'short_note', 'is_active', 'date_b', 'user_id', 'photo'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

	
}
