<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use  App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserData extends Model {
	protected $table = 'user_data';
	protected $fillable = [
		'phone', 'mobile', 'address', 'note', 'position', 'user_id'
	];
	
	use ChangeModel;
	use SoftDeletes;
	
}
