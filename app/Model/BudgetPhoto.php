<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;


class BudgetPhoto extends Model {
    use DateHelper, ChangeModel;

    protected $table = 'budget_photo';
    protected $fillable = ['budget_id', 'path', 'name', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
