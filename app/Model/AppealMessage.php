<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppealMessage extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'appeal_message';
    protected $fillable = [ 'name', 'note', 'file', 'is_owner', 'status_id', 'appeal_id', 'parent_id', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }
    
    
}
