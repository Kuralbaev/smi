<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LibBudgetExplain extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'lib_budget_explain';
    protected $fillable = ['name', 'user_id'];
    
    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
