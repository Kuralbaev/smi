<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LibOrganCat extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'lib_organ_cat';
    protected $fillable = ['user_id', 'name'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }
    
}
