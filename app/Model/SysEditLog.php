<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\SysEditLogRow;

use App\Services\Traits\DateHelper;

class SysEditLog extends Model {
    protected $table = 'sys_edit_log';
    use DateHelper;

	static function createNote($el){
        $user = \Auth::user();

        $ar_before_attr = $el->getDirty();
        $ar_new_attr = $el->getOriginal();


		if (isset($ar_before_attr['remember_token'])){
            unset($ar_before_attr['remember_token']);
        }
		if (isset($ar_before_attr['password'])){
            unset($ar_before_attr['password']);
        }

        if (!$user)
            return  true;

        $note = '';

        foreach ($ar_before_attr as $k=>$v){
            $note .= '<p>Поле "'.$k.'", значение до "'.$ar_new_attr[$k].', значение после "'.$v.'" </p>';
        }

        $log = new SysEditLog();
        $log->user_id = ($user ? $user->id : 0);
        $log->table_name = $el->getTable();
        $log->el_id = $el->id;
        $log->note = $note;
        $log->save();

        foreach ($ar_before_attr as $k=>$v){
            $row = new SysEditLogRow();
            $row->log_id = $log->id;
            $row->name = $k;
            $row->before = $ar_new_attr[$k];
            $row->after = $v;
            $row->save();
        }


    }

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

   

}
