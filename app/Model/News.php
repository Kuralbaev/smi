<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'news';
    protected $fillable = ['cat_id', 'name', 'short_note', 'note', 'photo', 'count_view', 'user_id', 'date_news'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
