<?php
namespace App\Model;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\SysAppealStatus;

class Appeal extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'appeal';
    protected $fillable = [ 'name', 'user_id', 'status_id', 'file', 'note', 'city_id', 'region_id', 'organ_id', 'control_organ_id', 'is_corrupt', 'is_public', 'is_public', 'budget_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relControlOrgan(){
        return $this->belongsTo('App\Model\Organ', 'control_organ_id');
    }

    function relOrgan(){
        return $this->belongsTo('App\Model\Organ', 'organ_id');
    }
    
    function relBudget(){
        return $this->belongsTo('App\Model\Budget', 'budget_id');
    }
	
	function relModuleRelation(){
		return $this->hasMany('App\Model\ModuleRelation', 'appeal_id');
    }
    
    function isClosed(){
        if ($this->status_id == SysAppealStatus::CLOSE_STATUS)
            return true;

        return false;
    }

    function relMessages(){
        return $this->hasMany('App\Model\AppealMessage', 'appeal_id');
    }


    function relClosedMessage(){
        $item = $this->relMessages()->where('status_id', SysAppealStatus::CLOSE_STATUS);
        return $item;
    }

    function getDiffClosedDays(){
        $d = new DateTime();

        return date_diff($d, new DateTime($this->created_at))->days;
    }
}
