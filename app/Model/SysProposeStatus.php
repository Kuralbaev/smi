<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SysProposeStatus extends Model {
    protected $table = 'sys_propose_status';
    
    CONST CREATE_STATUS = 1;
    CONST WORK_ACCEPT = 2;
    CONST WORK_APPROVE = 3;
    CONST CLOSE_VOTE = 4;
    CONST CLOSE_DECLINE = 5;

}
