<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsComment extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'news_comment';
    protected $fillable = ['news_id', 'title', 'note', 'photo', 'is_owner', 'is_positive', 'parent_id', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relAnswers(){
        return $this->hasMany('App\Model\NewsComment', 'parent_id')->orderBy('id', 'desc');
    }
}
