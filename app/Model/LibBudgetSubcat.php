<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LibBudgetSubcat extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'lib_budget_subcat';
    protected $fillable = ['user_id', 'name', 'cat_id'];
    
    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
