<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;

class BudgetDiff extends Model {
    use DateHelper, ChangeModel;
    protected $table = 'budget_diff';
    protected $fillable = [ 'el_id', 'k_avg', 'k_el', 'percent', 'is_full'];
    
}
