<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'company';
    protected $fillable = ['name', 'directors', 'photo', 'address', 'bin', 'phone', 'email', 'user_id'];
    
    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
