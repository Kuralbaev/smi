<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetOption extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'budget_option';
    protected $fillable = [ 'budget_id', 'sys_name', 'value_str', 'value_int', 'title', 'option_id'];
    
}
