<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Propose extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'propose';
    protected $fillable = [ 'name', 'photo', 'address', 'note', 'count_up', 'count_down', 'user_id', 'status_id', 'cat_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relAnswer(){
        return $this->hasOne('App\Model\ProposeAnswer', 'propose_id');
    }

}
