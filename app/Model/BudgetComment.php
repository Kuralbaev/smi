<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetComment extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'budget_comment';
    protected $fillable = ['budget_id', 'title', 'note', 'photo', 'youtube', 'is_owner', 'is_positive', 'parent_id', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relBudget(){
        return $this->belongsTo('App\Model\Budget', 'budget_id');
    }

    function getArPhoto(){
        if (!$this->photo)
            return [];
        return explode('|', $this->photo);

    }

    function relAnswers(){
        return $this->hasMany('App\Model\BudgetComment', 'parent_id')->orderBy('id', 'desc');
    }

    function getYoutube(){
        if (!$this->youtube)
            return false;

        $parts = parse_url($this->youtube);

        if (!isset($parts['query']))
            return false;

        parse_str($parts['query'], $query);
        if (!isset($query['v']))
            return false;

        return $query['v'];
    }
}
