<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SysBudgetStatus extends Model {
    protected $table = 'sys_budget_status';

    CONST PLAN_STATUS = 1;
    CONST DONE_STATUS = 2;
    CONST IN_WORK_STATUS = 3;
    CONST OVERDUE_STATUS = 4;
    CONST CANCEL_STATUS = 5;
    
}
