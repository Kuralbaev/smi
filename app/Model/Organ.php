<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organ extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'organ';
    protected $fillable = [ 'director_id', 'city_id', 'region_id', 'name', 'photo', 'geo_x', 'geo_y', 'cat_id', 'subcat_id', 'user_id', 'is_control', 'parent_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relDirector(){
        return $this->belongsTo('App\Model\Director','director_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

    function relCategory(){
        return $this->belongsTo('App\Model\LibOrganCat', 'cat_id')->withDefault([
            'name' => 'не указан'
        ]);
    }

    function relSubCat(){
        return $this->belongsTo('App\Model\LibOrganSubcat', 'subcat_id');
    }

    function relData(){
        return $this->hasOne('App\Model\OrganData', 'organ_id')->withDefault([
            'empty' => true
        ]);
    }

    function relParent(){
        return $this->belongsTo('App\Model\Organ', 'parent_id');
    }

    function relChilds(){
        return $this->hasMany('App\Model\Organ', 'parent_id');
    }


}
