<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SysAppealStatus extends Model {
    protected $table = 'sys_appeal_status';
    
    CONST CREATE_STATUS = 1;
    CONST DONE_STATUS = 2;
    CONST IN_WORK_STATUS = 3;
    CONST CLOSE_STATUS = 4;
    CONST RETURN_STATUS = 5;
}
