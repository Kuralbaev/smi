<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Director extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'directors';
    protected $fillable = ['name', 'phone', 'photo', 'user_id', 'address'];
    
    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
