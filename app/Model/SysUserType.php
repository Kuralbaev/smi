<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SysUserType extends Model {
    protected $table = 'sys_user_types';

    CONST ADMIN_TYPE = 1;
    CONST WORKER_TYPE = 2;
    CONST USER_TYPE = 3;
    CONST CHIEF_TYPE = 4;
    CONST WORKER_ORGAN_TYPE = 6;
    CONST WORKER_TERR_DEP_TYPE = 8;


}
