<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleRelation extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $table = 'module_relations';
    protected $fillable = [ 'news_id', 'propose_id', 'budjet_id', 'organ_id', 'director_id', 
                            'appeal_id', 'company_id', 'user_id', 'creator_id', 'from_field', 'from_id'];

    static function getFromModuleAr(){
        return [
            'user_id' => 'Пользователь',
            'news_id' => 'Новость',
            'propose_id' => 'Предложение',
            'budjet_id' => 'Бюджет',
            'organ_id' => 'Орган',
            'director_id' => 'Директор',
            'appeal_id' => 'Жалоба',
            'company_id' => 'Компания'
        ];
    }

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'empty' => true
        ]);
    }

    function relCreator(){
        return $this->belongsTo('App\User', 'creator_id')->withDefault([
            'empty' => true
        ]);
    }
    
    function relNews(){
        return $this->belongsTo('App\Model\News', 'news_id')->withDefault([
            'empty' => true
        ]);
    }

    function relPropose(){
        return $this->belongsTo('App\Model\Propose', 'propose_id')->withDefault([
            'empty' => true
        ]);
    }

    function relBudget(){
        return $this->belongsTo('App\Model\Budget', 'budjet_id')->withDefault([
            'empty' => true
        ]);
    }

    function relOrgan(){
        return $this->belongsTo('App\Model\Organ', 'organ_id')->withDefault([
            'empty' => true
        ]);
    }

    function relDirector(){
        return $this->belongsTo('App\Model\Director', 'director_id')->withDefault([
            'empty' => true
        ]);
    }

    function relAppeal(){
        return $this->belongsTo('App\Model\Appeal', 'appeal_id')->withDefault([
            'empty' => true
        ]);
    }

    function relCompany(){
        return $this->belongsTo('App\Model\Company', 'company_id')->withDefault([
            'empty' => true
        ]);
    }

}
