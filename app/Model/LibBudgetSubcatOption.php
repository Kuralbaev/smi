<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LibBudgetSubcatOption extends Model {
    use DateHelper, ChangeModel;
    use SoftDeletes;
    
    protected $table = 'lib_budget_sub_cat_option';
    protected $fillable = ['sub_cat_id', 'sys_key', 'name', 'is_int', 'user_id'];
    
    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
