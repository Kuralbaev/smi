<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;

class ProposeComment extends Model {
    use DateHelper, ChangeModel;
    protected $table = 'propose_comment';

    protected $fillable = [ 'propose_id', 'title', 'note', 'photo', 'is_owner', 'is_positive', 'parent_id', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }
}
