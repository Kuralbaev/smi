<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\DateHelper;
use App\Services\Traits\ChangeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysCity extends Model {
    protected $table = 'sys_city';
    use DateHelper, ChangeModel;
    use SoftDeletes;

    protected $fillable = [ 'name', 'parent_id', 'user_id'];

    function relUser(){
        return $this->belongsTo('App\User', 'user_id')->withDefault([
            'full_name' => 'не указан'
        ]);
    }

}
