<?php

namespace App\Policies\Admin\Lib;

use App\User;
use App\Model\SysUserType;
use Illuminate\Auth\Access\HandlesAuthorization;

class LibOrganPolicy {
    use HandlesAuthorization;

    public function __construct(){
        
    }

    private function mainCheck($user){
        return in_array($user->type_id, [SysUserType::ADMIN_TYPE,  SysUserType::WORKER_TYPE, SysUserType::CHIEF_TYPE, 
                                        SysUserType::WORKER_ORGAN_TYPE, SysUserType::WORKER_TERR_DEP_TYPE]);
    }

    public function list($user){
        if (!$this->mainCheck($user))
            return false;
        
        return true; 
    }

    public function view($user){
        if (!$this->mainCheck($user))
            return false;
        
        return true;
    }

    public function create($user){
        if (!$this->mainCheck($user))
            return false;
        
        return true;
    } 
    
    public function update($user, $item){
        if ($item->user_id == $user->id)
            return true;

        if (!in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::CHIEF_TYPE]))
            return false;

        return true;
    }

    public function delete($user, $item){
        if ( !in_array($user->type_id, [SysUserType::ADMIN_TYPE]))
            return false;

        return true;
    }
}
