<?php

namespace App\Policies\Admin\Organ;

use App\User;
use App\Model\SysUserType;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrganCommonPolicy {
    use HandlesAuthorization;

    public function __construct(){
        
    }

    private function mainCheck($user){
        return in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE, SysUserType::CHIEF_TYPE, 
                                        SysUserType::WORKER_ORGAN_TYPE, SysUserType::WORKER_TERR_DEP_TYPE]);
    }

    public function list($user){

        if (!$this->mainCheck($user))
            return false;
        
        return true; 
    }

    public function create($user){
        if ( !$this->mainCheck($user))
            return false;

        return true;
    } 
    
    public function update($user, $item){
        if ($user->type_id == SysUserType::WORKER_ORGAN_TYPE){
            $ar_organ = $user->getAccessOrgan();
            if (in_array($item->id, $ar_organ) || in_array($item->parent_id, $ar_organ))
                return true;
        }

        if ($user->type_id == SysUserType::WORKER_TERR_DEP_TYPE && $item->city_id == $user->city_id) 
            return true;

        if ( !in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE, SysUserType::CHIEF_TYPE]))
            return false;

        return true;
    }

    public function delete($user, $item){
        if ( !in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE, SysUserType::CHIEF_TYPE]))
            return false;

        return true;
    }
}
