<?php

namespace App\Policies\Admin\System;

use App\User;
use App\Model\SysUserType;
use Illuminate\Auth\Access\HandlesAuthorization;

class SystemControlPolicy {
    use HandlesAuthorization;

    public function __construct(){
        
    }

    private function mainCheck($user){
        return in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE, 
                                        SysUserType::CHIEF_TYPE, SysUserType::WORKER_TERR_DEP_TYPE]);
    }

    public function list($user){
        if (!$this->mainCheck($user))
            return false;
        
        return true; 
    }
   
    
    public function update($user, $item){
        if ($user->type_id == SysUserType::WORKER_TERR_DEP_TYPE && $user->city_id == $item->city_id) 
            return true;

        if ( !in_array($user->type_id, [SysUserType::ADMIN_TYPE, SysUserType::WORKER_TYPE,SysUserType::CHIEF_TYPE]))
            return false;

        return true;
    }
}
