if (document.getElementById("school_chart")) {
    var max_student = parseInt($('#school_chart').data('2'));
    var count_sudent = parseInt($('#school_chart').data('1'));
    Highcharts.chart('school_chart', {
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: ['Нормативная нагрузка', 'Фактическая нагрузка'],
            title: {
                enabled: false
            },
            labels: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                enabled: false
            }
        },
        tooltip: {
            formatter: function () {
                return this.x + ' - <b>' + this.y + '</b>';
            }
        },
        series: [{
            data: [{
                color: '#59a368',
                y: max_student
            }, {
                color: '#ff8c4e',
                y: count_sudent
            }],
            type: 'column'
        }]
    });
}

if (document.getElementById("road_chart")) {
    var max_student = parseInt($('#road_chart').data('2'));
    var count_sudent = parseInt($('#road_chart').data('1'));
    Highcharts.chart('road_chart', {
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: ['Средняя стоимость 1 километра', 'Фактическая стоимость 1 километра'],
            title: {
                enabled: false
            },
            labels: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                enabled: false
            }
        },
        tooltip: {
            formatter: function () {
                return this.x + ' - <b>' + this.y + '</b>';
            }
        },
        series: [{
            data: [{
                color: '#59a368',
                y: max_student
            }, {
                color: '#ff8c4e',
                y: count_sudent
            }],
            type: 'column'
        }]
    });
}

if (document.getElementById("avarage_chart")) {
    var max_student = parseInt($('#avarage_chart').data('2'));
    var count_sudent = parseInt($('#avarage_chart').data('1'));
    Highcharts.chart('avarage_chart', {
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: ['Средняя стоимость', 'Текущая стоимость'],
            title: {
                enabled: false
            },
            labels: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                enabled: false
            }
        },
        tooltip: {
            formatter: function () {
                return this.x + ' - <b>' + this.y + '</b>';
            }
        },
        series: [{
            data: [{
                color: '#59a368',
                y: max_student
            }, {
                color: '#ff8c4e',
                y: count_sudent
            }],
            type: 'column'
        }]
    });
}

if (document.getElementById("chart_subcat")) {
    Highcharts.chart('chart_subcat', {
        data: {
            table: 'chart_subcat_data'
        },
        
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: ''
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + numberWithSpaces(this.point.y) + ' </b><br/>' + this.point.name;
            }
        },
    });
}



function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}