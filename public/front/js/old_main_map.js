if (document.getElementById("map") && document.getElementById("ex2")) {
 
    ymaps.ready(init);
    function init(){ 
        // Создание карты.    
        var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчнию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [51.14345176, 71.44592914],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 13,
            controls: []
    
        });

       
        // change events
        $('#cat_id').change(function(){
            $('#subcat_id option').hide();
            $('#subcat_id option.js_subcat_option_'+$(this).val()).show();
            getPoints();


        })

        $('#explain_id').change(function(){
            getPoints();
        })
        $('#subcat_id').change(function(){
            getPoints();
        })
        $('#status_id').change(function(){
            getPoints();
        })
        $('#ex2').slider().on('slideStop', function(ev){
            getPoints();
            let ar_val = $('#ex2').data('slider').getValue();
            
            $('#js_from_price').html(thousandSeparator(ar_val[0])+' тг.');
            $('#js_to_price').html(thousandSeparator(ar_val[1])+' тг.');
        });
        
        
        $('.js_change_map_object').change(function(){
            getPoints();
        })

        var data_budget = [];
        function getPoints(){
            let cat_id = $('#cat_id').val();
            let subcat_id = $('#subcat_id').val();
            let explain_id = $('#explain_id').val();
            let status_id = $('#status_id').val();
            let sum = $('#ex2').val();

            let cart_name = $('#cart_name').val();
            let date_b = $('#date_b').val();
            let date_e = $('#date_e').val();
            let company_id = $('#company_id').val();
            let organ_id = $('#organ_id').val();
            let address_id = $('#address_id').val();


            $.post( "/data", {
                cat_id: cat_id,
                subcat_id: subcat_id,
                explain_id: explain_id,
                status_id: status_id,
                sum: sum,
                
                cart_name: cart_name,
                date_b: date_b,
                date_e: date_e,
                company_id: company_id,
                organ_id: organ_id,
                address_id: address_id,
            }).done(function( data ) {
                data_budget = calculateAvarageSumm(data);

                parseBudgetData(data_budget);
            });
        }
        getPoints();

        var cat_avarage = [];
        function calculateAvarageSumm(data){
            $.each( data.data, function( key, item ) {
                //console.log(item);
                if (cat_avarage[item.subcat_id+'_'+item.explain_id] == undefined)
                    cat_avarage[item.subcat_id+'_'+item.explain_id] = {
                        count : 0,
                        summ : 0
                    };
                
                cat_avarage[item.subcat_id+'_'+item.explain_id]['summ'] += item.summ;
                cat_avarage[item.subcat_id+'_'+item.explain_id]['count'] += 1;
            });


            return data;
        }

        function parseBudgetData(data){
            myMap.geoObjects.removeAll();

            // показать сслыку 
            $('.js_max_summ .link').hide();
            if (data.max_item){
                $('.js_max_summ .link').show();
                $('.js_max_summ .link').data('id', data.max_item.id);
            }

            $('.js_min_summ .link').hide();
            if (data.min_item){
                $('.js_min_summ .link').show();
                $('.js_min_summ .link').data('id', data.min_item.id);
            }

            // заполнение средних сумм
            let text = Math.ceil(data.avarage_sum/1000000)+' млн.тг.';
            writeTextByJS('js_avarage_summ', text, 100);
            
            text = Math.ceil(data.max_sum/1000000)+' млн.тг.';
            writeTextByJS('js_max_summ', text, 100);

            text = Math.ceil(data.min_summ/1000000)+' млн.тг.';
            writeTextByJS('js_min_summ', text, 100);

            $.each( data.data, function( key, item ) {
                if (item.is_road)
                    addRoadToMap(item);
                else 
                    addPointToMap(item);
            }); 
        }


        // Creating a content layout.
        HelthnContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="yand_icon" style=""><span class="fa fa-medkit">$[properties.iconContent]</div>'
        )
        SchoolContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="yand_icon" style=""><span class="fa fa-graduation-cap">$[properties.iconContent]</div>'
        )
        RoadContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="yand_icon" style=""><span class="fa fa-road">$[properties.iconContent]</div>'
        )
        BuildContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="yand_icon" style=""><span class="fa fa-building">$[properties.iconContent]</div>'
        )
        CultureContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="yand_icon" style=""><span class="fa fa-music">$[properties.iconContent]</div>'
        )

        function addRoadToMap(item){
            ar_coord = JSON.parse(item.geo_ar);

            summ_str = Math.ceil(item.summ / 1000000);
            summ_str = thousandSeparator(summ_str.toString());

            let abarage_summ = cat_avarage[item.subcat_id+'_'+item.explain_id];
            abarage_summ = Math.ceil(abarage_summ.summ / abarage_summ.count);
            
            let koef = ((item.summ/abarage_summ) * 100) - 100;

            

            iconImageHref = '/front/img/icon/square.png'
            if (koef > 150)
                iconImageHref = '/front/img/icon/square_red.png'
            else if (koef > 100) 
                iconImageHref = '/front/img/icon/square_orange.png'
            else 
                iconImageHref = '/front/img/icon/square_green.png'

            let icon = RoadContentLayout
            if (item.cat_id == 7)
                icon = HelthnContentLayout
            else if (item.cat_id == 10) 
                icon = SchoolContentLayout
            else if (item.cat_id == 13)
                icon = BuildContentLayout
            else if (item.cat_id == 14)
                icon = CultureContentLayout


            ar_coord_point = ar_coord[0];
            place = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: ar_coord_point
                },
                properties: {
                    iconContent: "&nbsp;" + summ_str + " млн",
                }
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: iconImageHref,
                iconImageSize: [90, 40],
                iconImageOffset: [-45, -20],
                iconContentOffset: [0, 0],
                iconContentSize: [90, 40],
                iconContentLayout: icon
            });
            myMap.geoObjects.add(place);
            place.db_item = item
            place.events.add("click", clickOnPoint, place);
            

            geo = new ymaps.GeoObject({
                geometry: {
                    type: "LineString",
                    coordinates: ar_coord
                }
            }, {
                strokeWidth: 5,
                strokeColor: "#5684ac"
            });
            myMap.geoObjects.add(geo);

        }
        

        function addPointToMap(item){
            ar_coord = [item.geo_x, item.geo_y];
            
            summ_str = Math.ceil(item.summ / 1000000);
            summ_str = thousandSeparator(summ_str.toString());

            let abarage_summ = cat_avarage[item.subcat_id+'_'+item.explain_id];
            abarage_summ = Math.ceil(abarage_summ.summ / abarage_summ.count);
            
            let koef = ((item.summ/abarage_summ) * 100) ;
            
            if (item.id == 61){
                console.warn(item, koef, abarage_summ);
            }

            iconImageHref = '/front/img/icon/square.png'
            if (koef > 150)
                iconImageHref = '/front/img/icon/square_red.png'
            else if (koef > 110) 
                iconImageHref = '/front/img/icon/square_orange.png'
            else 
                iconImageHref = '/front/img/icon/square_green.png'

            let icon = RoadContentLayout
            if (item.cat_id == 7)
                icon = HelthnContentLayout
            else if (item.cat_id == 10) 
                icon = SchoolContentLayout
            else if (item.cat_id == 13)
                icon = BuildContentLayout
            else if (item.cat_id == 14)
                icon = CultureContentLayout
            
            place = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: ar_coord
                },
                properties: {
                    iconContent: "&nbsp;" + summ_str + " млн",
                }
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: iconImageHref,
                iconImageSize: [90, 40],
                iconImageOffset: [-45, -20],
                iconContentOffset: [0, 0],
                iconContentSize: [90, 40],
                iconContentLayout: icon
            });

            myMap.geoObjects.add(place);
            place.db_item = item
            place.events.add("click", clickOnPoint, place);
        }

        function clickOnPoint(item){
            item = item.get('target');

            let card = $('#map_card');
            card.addClass('map_card--open map_card--loader');
            $.post( "/item/" + item.db_item.id).done(function( data ) {
                card.removeClass('map_card--loader');
                card.html(data)
            });

        }

        $('.js_max_summ .link').click(function(){
            let id = $(this).data('id');

            let card = $('#map_card');
            card.addClass('map_card--open map_card--loader');
            $.post( "/item/" + id).done(function( data ) {
                card.removeClass('map_card--loader');
                card.html(data)
            });

        });

        $('.js_min_summ .link').click(function(){
            let id = $(this).data('id');

            let card = $('#map_card');
            card.addClass('map_card--open map_card--loader');
            $.post( "/item/" + id).done(function( data ) {
                card.removeClass('map_card--loader');
                card.html(data)
            });

        });

        $('#map_card').on('click', '.map_card__close', function() {
            
            $('#map_card').removeClass('map_card--open');
            $('#map_card').removeClass('map_card--loader');
        });

        

        
        $('#map_card').on('click', '.js_call_appeal', function() {
            let id = $(this).data('id');

            let form = $('#appeal_modal').closest( "form" );
            form.attr('action', '/budget/'+id+'/create-appeal');

            $('#appeal_modal').modal('show')
        });

        var thousandSeparator = function(str) {
            var parts = (str + '').split('.'),
                main = parts[0],
                len = main.length,
                output = '',
                i = len - 1;
        
            while(i >= 0) {
               

                output = main.charAt(i) + output;
                if ((len - i) % 3 === 0 && i > 0) {
                    output = ' ' + output;
                }
                --i;
            }
        
            if (parts.length > 1) {
                output += '.' + parts[1];
            }
            return output;
        };

        function writeTextByJS(id, text, speed){
            var ele = document.getElementById(id),
                txt = text.split("");
            
            ele.innerHTML = '';
            var interval = setInterval(function(){
        
                if(!txt[0]){
        
                    return clearInterval(interval);
                };
        
                ele.innerHTML += txt.shift();
            }, speed != undefined ? speed : 100);
        
            return false;
        };

        $('.filter__link').click(function(){
            let icon = $(this).find('.fa');
            
            if (icon.hasClass('fa-chevron-down')){
                icon.removeClass('fa-chevron-down')
                icon.addClass('fa-chevron-up')

                $('.js_show_dop_filter').show();
            }
            else {
                icon.removeClass('fa-chevron-up')
                icon.addClass('fa-chevron-down')

                $('.js_show_dop_filter').hide();
            }

        });

        $('.js_show_dop_filter').hide();
    }
    
    
};

