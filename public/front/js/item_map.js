if (document.getElementById("map_item")){
    ymaps.ready(init);
    function init(){ 
        
        // Создание карты.    
        let el = $('#map_item');
        if (el.data('type') == 1 || el.data('type') == '1'){
            ar_coord = el.data('data_json');

            ar_coord_point = ar_coord[0];

            var myMap = new ymaps.Map("map_item", {
                center: ar_coord_point,
                zoom: 13,
                controls: []
            });

            place = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: ar_coord_point
                }
            });
            myMap.geoObjects.add(place);
            

            geo = new ymaps.GeoObject({
                geometry: {
                    type: "LineString",
                    coordinates: ar_coord
                }
            }, {
                strokeWidth: 2,
                strokeColor: "#3c6a93"
            });
            myMap.geoObjects.add(geo);
        }
        else {
            ar_coord = [parseFloat(el.data('x')), parseFloat(el.data('y'))];
            
            var myMap = new ymaps.Map("map_item", {
                center: ar_coord,
                zoom: 13,
                controls: []
            });

            place = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: ar_coord
                }
            });
            myMap.geoObjects.add(place);
        }
    }
}
