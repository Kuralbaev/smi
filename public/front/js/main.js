



$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

jQuery(function ($) {
    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $(".share__list"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            &&
            div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.share_sidebar').removeClass('open'); // удаляем класс
        }
    });
});



$('.c_group input').keyup(function(){
    
    if ($(this).val() == ''){
        $(this).parent().removeClass('c_group--fill');
        return;
    }
    
    $(this).parent().addClass('c_group--fill');
})

$( ".c_group input" ).each(function( index ) {
    if ($(this).val() == ''){
        $(this).parent().removeClass('c_group--fill');
        return;
    }
    
    $(this).parent().addClass('c_group--fill');
});

$('.c_group textarea').keyup(function(){
    
    if ($(this).val() == ''){
        $(this).parent().removeClass('c_group--fill');
        return;
    }
    
    $(this).parent().addClass('c_group--fill');
})

$( ".c_group textarea" ).each(function( index ) {
    if ($(this).val() == ''){
        $(this).parent().removeClass('c_group--fill');
        return;
    }
    
    $(this).parent().addClass('c_group--fill');
});

if ($('.modal_mess').length > 0) {
    console.log('modal_mess');
    $('.modal_mess').modal('show')
}


$('.c_group__file:not(.many)').change(function(){
    if (this.files && this.files[0]) {
        var preveiw = $(this).parent().find('.c_group__preview');
        console.log(preveiw);
        var reader = new FileReader();
        
        reader.onload = function(e) {
            preveiw.attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
});

$('.c_group__file.many').change(function(){
    console.log(this.files);
    var content = $(this).parent().find('.c_group__image_note');
    content.html('Загружено '+this.files.length+' изображений');


});


if (document.getElementById("ex3")) {
    $('#ex3').slider().on('slideStop', function(ev){
        let ar_val = $('#ex3').data('slider').getValue();
        
        $('#js_from_price_3').html(thousandSeparator(ar_val[0])+' тг.');
        $('#js_to_price_3').html(thousandSeparator(ar_val[1])+' тг.');
    });
}

var thousandSeparator = function(str) {
    var parts = (str + '').split('.'),
        main = parts[0],
        len = main.length,
        output = '',
        i = len - 1;

    while(i >= 0) {
       

        output = main.charAt(i) + output;
        if ((len - i) % 3 === 0 && i > 0) {
            output = ' ' + output;
        }
        --i;
        $(".rotate").click(function () {
    $(this).toggleClass("down");
})
    }

    if (parts.length > 1) {
        output += '.' + parts[1];
    }
    return output;
};


// Burger

let any = (a,b) => { return "number: "+(a+b);}
console.log(any(50,5));

$(document).ready( function () {
    $('.nav-icon3').click(function () {
        $('.nav-icon3').toggleClass('open');
        $('#sidebar').toggleClass('opened');
        $('#sidebar').toggleClass('open');
        $(".rotate").removeClass("down");
        $('#bottom').removeClass("opend");
        $('.menu_list_show__a').toggleClass('open');
        $('.menu_list_show').toggleClass('open');
    });
    $('.open_back').click(function(){
        $('.menu_list_show__a').toggleClass('open');
        $('.menu_list_show').toggleClass('open');
        $('.nav-icon3').toggleClass('open');
        $('#sidebar').toggleClass('open');
    });
    $(".rotate").click(function () {
        $(this).toggleClass("down");
        $('#bottom').toggleClass("opend");
        $('.nav-icon3').removeClass('open');
        $('#sidebar').removeClass('opened');
    });

    $("#logo_inside_click").click(function () {
        $('.logo_inside_beta').css({'display':'block'});
    });
    $("#logo_click").click(function () {
        $('.logo_inside_beta').css({ 'display': 'block' });
    });
    $(".logo_inside_beta_close").click(function () {
        $('.logo_inside_beta').css({ 'display': 'none' });
    });


    $('.dropdawn_menu').click(function(){
        $(this).toggleClass('open');
    });
    $('.share__title').click(function(){
        $('.share_sidebar').toggleClass('open');
    });
});


// Preloadre

$(window).on('load', function () {
    $preloader = $('.loaderArea'),
    $loader = $preloader.find('.loader');
    $loader.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});

// Server Worker


$(document).ready(function() {
    $('.pgwSlider').pgwSlider({
        autoSlide:false,
        adaptiveHeight: true
    });
});

if (window.location.href == 'http://adgs.test/budget-list') {
    
}   

$('.pronamic-wp-posts-example').select2({
    minimumResultsForSearch: -1
});

$('.multiple-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    responsive: [{
        breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


$('.close_r').click(function (){
    $('.answers').toggleClass('close');
    $('.close_r').innerHTML = 'Ответы';
});

$(function () {

    $('#dropzone').on('dragover', function () {
        $(this).addClass('hover');
    });

    $('#dropzone').on('dragleave', function () {
        $(this).removeClass('hover');
    });

    $('#dropzone input').on('change', function (e) {
        var file = this.files[0];

        $('#dropzone').removeClass('hover');

        if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }

        $('#dropzone').addClass('dropped');
        $('#dropzone img').remove();        
    });
});


$('#dropzone [type="file"]').on('dragenter', function () {
    // $('#dropzone').css({'transform' : 'scale(1.05)'});
    $('#dropzone p').css({ 'opacity': '0.5' });
    $('#dropzone').css({ 'border': '2px dashed #17a2b8' });
    $('#dropzone').css({ 'background': '#17a2b814' });

});
$('#dropzone [type="file"]').on('dragleave', function () {
    $('#dropzone').css({ 'transform': 'scale(1)' });
    $('#dropzone p').css({ 'opacity': '1' });
    $('#dropzone').css({ 'border': '2px dashed #d0d0d0' });
    $('#dropzone').css({ 'background': 'none' });
});