
function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}



Highcharts.chart('chart_pie', {
    data: {
        table: 'chart_data'
    },
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Units'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + numberWithSpaces(this.point.y) + ' ('+Math.round(this.point.percentage)+'%) </b><br/>' + this.point.name;
        }
    },
});




Highcharts.chart('chart_explain', {
    data: {
        table: 'chart_explain_data'
    },
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Units'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + numberWithSpaces(this.point.y) + ' ('+Math.round(this.point.percentage)+'%) </b><br/>' + this.point.name;
        }
    },
});


Highcharts.chart('chart_cat', {
    data: {
        table: 'chart_cat_data'
    },
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    legend: {
        enabled: false
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: ''
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + numberWithSpaces(this.point.y) + '  </b><br/>' + this.point.name;
        }
    },
});

Highcharts.chart('chart_subcat', {
    data: {
        table: 'chart_subcat_data'
    },
    
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    legend: {
        enabled: false
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: ''
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + numberWithSpaces(this.point.y) + ' </b><br/>' + this.point.name;
        }
    },
});