const cacheName = 'v1';
const cacheAssets = [
    './index.php',
    './css/add.css',
    './css/medis.css',
    './css/style.boundle.css',
    './js/main.js'
];


// Call Install Event
self.addEventListener('install', e => {
    console.log('Service Worker: Installed');

    e.waitUntil(
        caches
            .open(cacheName)
            .then(cashe => {
                console.log('Service Worker: Caching Files');
                cacheAssets.addAll(cacheAssets);
            })
            .then(() => self.skipWaiting())
    );
});

// Call Activate Event
self.addEventListener('activate', e => {
    console.log('Service Worker: Activate');

    e.waitUntil(
        caches.keys().then(cacheName => {
            return Promise.all(
                cacheName.map(cache => {
                    if(cache !== cacheName){
                        console.log('Service Worker: Clearing Old Cache');
                        return caches.delete(cache);
                    }
                })
            )
        })
    );
});

// Call Fetch Event

self.addEventListener('fetch', e =>{
    console.log('Service Worker: Fetching');
    e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
})