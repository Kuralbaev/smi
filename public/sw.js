"use strict";
importScripts('sw-toolbox.js'); 
toolbox.precache(['/','front/css/style.bundle.css']); 
toolbox.router.get('/images/*', toolbox.cacheFirst); 
toolbox.router.get('/*', toolbox.networkFirst, { networkTimeoutSeconds: 5});